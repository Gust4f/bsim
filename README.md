BSim
====

Chaotic Traffic Simulator simulates the chaos that one could find
in traffic by spawning cars with personalities that can change based
on environmental variables that the user controls and give snickers
to the driver if the driver is getting too hungry and stressed and 
turn into a driving diva.

This project uses and displays the concurrency of the world in a fun
and chaotic way where chaos is actually seen as the objective to 
acheive by the user running the program. It is also possible to 
solve deadlocks that occur in multiple ways and sometimes the
personalities of the drivers solve it by themselves based on their
configurations.

There are no legal issues with this application if the MIT license is followed.

